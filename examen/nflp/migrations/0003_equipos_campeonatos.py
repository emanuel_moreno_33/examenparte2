# Generated by Django 2.2.4 on 2019-09-14 02:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nflp', '0002_auto_20190914_0244'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipos',
            name='campeonatos',
            field=models.IntegerField(default=0),
        ),
    ]
