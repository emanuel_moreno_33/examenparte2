
from django import forms
from .models import jugador,equipos,estadios
class jugadorform(forms.ModelForm):
	class Meta:
			model = jugador
			fields=[
				"nombre",
				"apellido",
				"apodo",
				"numero",
				"equipo",
				"posicion",
				"status"
			]

class equipoform(forms.ModelForm):
	class Meta:
			model = equipos
			fields = [
				"nombre",
				"apodo",
				"lugar_origen"
			]

class estadiosadmin(forms.ModelForm):
	class Meta:
			model = estadios
			fields = [
				"nombre",
				"lugar_origen",
				"equipo"
			]
		