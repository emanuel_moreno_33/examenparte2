from django.contrib import admin

from .models import jugador,equipos,estadios

class adminjugador(admin.ModelAdmin):
	list_display=('nombre','apellido','numero')
	list_filter=('nombre','apellido','numero')

admin.site.register(jugador,adminjugador)


class adminequipos(admin.ModelAdmin):
	list_display=('nombre','lugar_origen')
	list_filter=('nombre','lugar_origen')
admin.site.register(equipos,adminequipos)

class adminestadios(admin.ModelAdmin):
	list_display=('nombre','equipo')
	list_filter=('nombre','equipo')

admin.site.register(estadios,adminestadios)